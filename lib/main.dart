import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'BottomAppBar v1',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        elevation: 0.0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('F34th3R')
          ],
        ),
      ),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('v-1.0')
            ],
          ),
        ),
      ),
      bottomNavigationBar: FthBottomNavBar(),
    );
  }
}

class FthBottomNavBar extends StatefulWidget {
  @override
  _FthBottomNavBarState createState() => _FthBottomNavBarState();
}

class _FthBottomNavBarState extends State<FthBottomNavBar> {

  int selected = 0;
  List<NavigationItem> items = [
    NavigationItem(
        Icon(Icons.home, color: Colors.white,),
//        Text('Home', style: TextStyle(color: Colors.white),)
        'Home',
        Colors.indigoAccent
    ),
    NavigationItem(
        Icon(Icons.favorite, color: Colors.white,),
//        Text('Favorite', style: TextStyle(color: Colors.white),)
        'Favorite',
        Colors.pinkAccent
    ),
    NavigationItem(
        Icon(Icons.search, color: Colors.white,),
//        Text('Search', style: TextStyle(color: Colors.white),)
        'Search',
        Colors.lime
    ),
    NavigationItem(
        Icon(Icons.more_vert, color: Colors.white,),
//        Text('Settings', style: TextStyle(color: Colors.white),)
        'Settings',
        Colors.cyan
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 12.0, right: 12.0),
      height: 55.0,
      width: MediaQuery.of(context).size.width,
      color: Colors.black,
//      * If the bottomAppBar is white with shadow
//      decoration: BoxDecoration(
//        color: Colors.white,
//        boxShadow: [
//          BoxShadow(
//            color: Colors.black12,
//            blurRadius: 3.0
//          ),
//        ]
//      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: items.map((item) {

          var itemIndex = items.indexOf(item);
          return GestureDetector(
            onTap: () {
              setState(() {
                selected = itemIndex;
              });
            },
            child: _buildItem(item, selected == itemIndex),
          );
        }).toList(),
      ),
    );
  }

  Widget _buildItem(NavigationItem item, bool isSelected) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 270),
      padding: isSelected ? EdgeInsets.only(left: 8.0, right: 6.0) : null,
      height: 44.0, //* double.maxFinite
      width: isSelected ? 120.0 : 50.0,
      decoration: isSelected ? BoxDecoration(
        color: item.color,
        borderRadius: BorderRadius.all(Radius.circular(34.0))
      ) : null,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Row(
            children: <Widget>[
              IconTheme(
                data: IconThemeData(
                    size: 24.0
                ),
                child: item.icon,
              ),
              Padding(
                padding: EdgeInsets.only(left: 8.0),
                child: isSelected ? Text(
                    item.title,
                    style: TextStyle(color: Colors.white)
                ) : Container(),
              )
            ],
          )
        ],
      ),
    );
  }
}

class NavigationItem {
  final Icon icon;
  final String title;
  final Color color;
  NavigationItem(this.icon, this.title, this.color);
}

